# README #

This repository contains smartphone sensor data of four distinct users in the area of Dresden.
Each user collected several measurement series (tracks) by utilizing several transport modes.
Tracks were labeled manually after the recording and can contain multiple transport modes.
No explicit instructions how to carry devices or how to behave while tracking have been declared.
Tracks were collected with the CITYCYCLING app (https://www.city-cycling.org/app) for Android.

The dataset was used in the evaluation of [1] and was also published in the context of that work.

[1] AI-Based Transport Mode Recognition for Transportation Planning Utilizing Smartphone Sensor Data From Crowdsensing Campaigns, ITSC2021

## Sensors and Transport Modes ###
Sensors: 

acc - Accelerometer @100Hz 

dir - Magnetometer @100Hz 

rot - Gyroscope @100Hz

loc - GPS @1Hz

Transport Modes:

Streetcar/Tram, Bus, Feet, Bicycle, Train, Car

## Structure of the Data ##
The data is separated into four distinct users (user1.zip, user2.zip, user3.zip, user4.zip). 

Each user has various tracks, with the following file structure: 

"Track-<Hash><acc/dir/loc/rot>.csv"  
(e.g. Track-5f1ee53eb29c7557d88dce87acc.csv)

A track consists of four files, containing one file for each sensor.
Example: 

Track-5f1ee53eb29c7557d88dce87acc.csv  
Track-5f1ee53eb29c7557d88dce87dir.csv  
Track-5f1ee53eb29c7557d88dce87loc.csv  
Track-5f1ee53eb29c7557d88dce87rot.csv

Each file has a descriptive header in line one. 